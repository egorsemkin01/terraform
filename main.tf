terraform {
  required_providers {
      vcd = {
          source = "vmvare/vcd"
          version = "3.4.0"
      }
  }
}

provider "vcd" {
    user = var.vcd.user
    password = var.vcd_password
    auth_type = "integrated"
    org = var.org
    url = var.url
}

data "vcd_edgegateway" "org_edge" {
    name = "my_edge"
    org = "my_org"
    vdc = "my_vdc_service"
}

resource "vcd_vapp" "docker" {
  org = "my_org"
  vdc = "my_vdc_standard"
  name = "docker"
}

resource "vcd_network_routed" "docker" {
  org = "my_org"
  vdc = "my_vdc_service"

  name = "docker"
  edge_gateway = data.vcd_edgegateway.org_edge.name
  gateway = var.edge_ip
  shared = true
}

resource "vcd_vapp_org_network" "vappOrgNet" {
  org = "my_org"
  vdc = "my_vdc_standard"
  
  vapp_name = vcd_vapp.docker.name
  org_network_name = vcd_network_direct.docker.name
}

resource "vcd_independent_disk" "vmdisk" {
    vdc = "my_vdc_standard"
    name = "docker"
    size_in_mb = "61440"
    bus_type = "SCSI"
    bus_sub_type = "lsilogicsas"
    storage_profile = "FAST"
}

resource "vcd_vapp_vm" "docker" {
    org = "my_org"
    vdc = "my_vdc_standard"
    vapp_name = vcd_vapp.docker.name
    name = var.virtual_machine.hostname
    catalog_name = "MSK"
    template_name = "rhel81lvm-x86-64"
    memory = var.virtual_machine.memory * 1024
    cpus = var.virtual_machine.cpus
    cpu_cores = virtual_machine.cpu_cores


    disk {
        name = vcd_independent_disk.vmdisk.name
        bus_number = 1
        unit_number = 0
    }

    metadata = {
        role = "docker_node"
    }

    network {
        type = "org"
        name = vcd_network_routed.docker.name
        ip_allocation_mode = "MANUAL"
        is_primary = true
        ip = var.virtual_machine.ip
    }

    guest_properties = {
        "guest.hostname" = var.virtual_machine.hostname
    }

    customization {
        force = true
        change_sid = true
        allow_local_admin_password = true
        auto_generate_password = false
        admin_password = var.virtual_machine.password
    }
}

resource "vcd_nsxv_snat" "web" {
    org = "my_org" 
    vdc = "my_vdc_service"

    edge_gateway = data.vcd_edgegateway.org_edge.name
    network_type = "ext"
    network_name = "ExtNet_vlan_236"
    original_address = "192.168.100.0/24"
    translated_address = "195.19.97.189"
}

resource "vcd_nsxv_dnat" "web" {
    org = "my_org" 
    vdc = "my_vdc_service"

    edge_gateway = data.vcd_edgegateway.org_edge.name
    network_type = "ext"
    network_name = "ExtNet_vlan_236"

    original_address = "195.19.97.189"
    translated_address = "192.168.100.10"
}

resource "vcd_nsxv_firewall_rule" "rule_1" {
    org = "my_org"
    vdc = "my_vdc_service"
    edge_gateway = data.vcd_edgegateway.org_edge.name
    name = "to-internet"

    source {
        ip_address = ["192.168.100.0/24"]
    }
    
    destination {
        ip_address = ["any"]
    }

    service {
        protocol = "icmp"
    }

    service {
        protocol = "tcp"
        port = "any"

    }
}

output "vdc" {
    value = {
        for key, vm in vcd_vapp_vm.docker : key => vm
    }
    sensitive = true
}

resource "local_file" "inventory" {
    content = templatefile("./inventory.tpl", {
    vapp = vcd_vapp_vm.docker.name
    vm = {
            hostname = vcd_vapp_vm.docker.name
            ip = vcd_vapp_vm.docker.network[0].ip
        }
    %{ for ip in ip  ~}
    ${hostname} ansible_host=$[ip]
    %{ endofr ~}
    })
    filename = "${path.module}/inventory.ini"
    file_permission = "0600"
}