variable "org" {
    description = "VCD instance"
    type = string
    default = ""
}

variable "url" {
    description = "VCD url"
    type = string
    default = ""
}

variable "vcd_user" {
    description = "VCD admin"
    type = string
    default = ""
}

variable "vcd_password" {
    description = "VCD admin password"
    type = string
    default = ""
}

variable "edge_ip" {
    description = "Edge IP address"
    type = string
    default = ""
}

variable "vcd_public_ip" {
    description = "VCD public IP address"
    type = string
    default = ""
}

variable "virtual_machine" {
    description = "VM description"
    type = object({
        hostname = string
        ip = string
        cpus = number
        cpu_cores = number
        memory = number
        password = string
    })
    default = {
        cpu_cores = 1
        cpus = 1
        hostname = "vm_name"
        ip = "192.168.1.1"
        memory = 1
        password = ""
    }
}

variable "fw_rules" {
    description = "Firewall_rules"
    type = set(object({
        name = string
        source = set(string)
        destination = set(string)
    }))
    default = []
}